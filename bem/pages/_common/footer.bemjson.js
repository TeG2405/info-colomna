module.exports = [
    {block: 'footer', content: [
        {mix: {block: 'container'}, content: [
            {block: 'columns', content: [
                {elem: 'item', content: [
                    {block: 'h', size: 4, mix: {block: 'footer', elem: 'title'}, content: [
                        {block: 'a', content: 'Для бизнеса и компаний'}
                    ]},
                    {block: 'list-unstyled', mix: {block: 'footer', elem: 'list'}, content: [
                        {block: 'a', content: 'Добавить предприятие'}
                    ]}
                ]},
                {elem: 'item', content: [
                    {block: 'h', size: 4, mix: {block: 'footer', elem: 'title'}, content: [
                        {block: 'a', content: 'Новости'}
                    ]},
                    {block: 'list-unstyled', mix: {block: 'footer', elem: 'list'}, content: [
                        {block: 'a', content: 'Новости города'},
                        {block: 'a', content: 'ТОП новости'},
                        {block: 'a', content: 'Подписаться на новости'},
                        {block: 'a', content: 'Сообщить новость'}
                    ]}
                ]},
                {elem: 'item', content: [
                    {block: 'h', size: 4, mix: {block: 'footer', elem: 'title'}, content: [
                        {block: 'a', content: 'Досуг'}
                    ]},
                    {block: 'list-unstyled', mix: {block: 'footer', elem: 'list'}, content: [
                        {block: 'a', content: 'Афиша города'},
                        {block: 'a', content: 'Сегодня в кино'},
                        {block: 'a', content: 'Вечеринки и концерты'},
                        {block: 'a', content: 'Фотоотчеты'},
                        {block: 'a', content: 'Сообщить о событии'}
                    ]}
                ]},
                {elem: 'item', content: [
                    {block: 'h', size: 4, mix: {block: 'footer', elem: 'title'}, content: [
                        {block: 'a', content: 'Каталог'}
                    ]},
                    {block: 'list-unstyled', mix: {block: 'footer', elem: 'list'}, content: [
                        {block: 'a', content: 'Предприятий'}
                    ]}
                ]},
                {elem: 'item', content: [
                    {block: 'h', size: 4, mix: {block: 'footer', elem: 'title'}, content: [
                        {block: 'a', content: 'Частные объявления'}
                    ]},
                    {block: 'list-unstyled', mix: {block: 'footer', elem: 'list'}, content: [
                        {block: 'a', content: '4 объявления'},
                        {block: 'a', content: 'Добавить Объявления'}
                    ]}
                ]},
                {elem: 'item', content: [
                    {block: 'h', size: 4, mix: {block: 'footer', elem: 'title'}, content: [
                        {block: 'a', content: 'Мой аккаунт'}
                    ]},
                    {block: 'list-unstyled', mix: {block: 'footer', elem: 'list'}, content: [
                        [
                            {block: 'a', content: 'Регистрация'},
                            ' / ',
                            {block: 'a', content: 'Войти'}
                        ],
                        {block: 'a', content: 'Восстановить пароль'}
                    ]}
                ]}
            ]}
        ]},
        {elem: 'dividing', content: [
            {mix: {block: 'container'}, content: [
                {mix: {block: 'row'}, content: [
                    {mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 6}}, content: [
                        {block: 'social', content: [
                            {elem: 'title', content: 'Присоединяйтесь к нам'},
                            {block: 'list-inline', mix: {block: 'social', elem: 'list'}, content: [
                                {block: 'a', content: {block: 'fa', icon: 'vk'}},
                                {block: 'a', content: {block: 'fa', icon: 'facebook'}},
                                {block: 'a', content: {block: 'fa', icon: 'twitter'}},
                                {block: 'a', content: {block: 'fa', icon: 'odnoklassniki'}}
                            ]}
                        ]}
                    ]},
                    {mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 6}}, content: [
                        {block: 'list-inline', mix: {block: 'mbn'}, content: [
                            {block: 'a', content: {block: 'img', src: 'http://placehold.it/120x42'}},
                            {block: 'a', content: {block: 'img', src: 'http://placehold.it/120x42'}}
                        ]},
                    ]}
                ]}
            ]}
        ]},
        {mix: {block: 'container'}, content: [
            {elem: 'bottom', content: [
                {elem: 'include', content: [
                    '2017 © infokolomna.ru - Сайт города Коломны',
                    {block: 'br'},
                    {block: 'a', content: 'Политика конфиденциальности'}
                ]},
                {elem: 'include', content: [
                    'г. Коломна, ул. Октябрьской революции, 368, оф. 20',
                    {block: 'br'},
                    {block: 'a', content: 'info@infokolomna.ru'},
                    ', ',
                    {block: 'a', content: '+7 (926) 883-95-97'},
                    ', ',
                    {block: 'a', content: '+7 (925) 152-41-34'}
                ]},
                {elem: 'include', mods: {type: 'right'}, content: [
                 
                    {block: 'a', content: [
                        {block: 'img', src: 'http://placehold.it/136x36'}
                    ]},
                    {block: 'a', content: [
                        {block: 'img', src: 'http://placehold.it/136x36'}
                    ]},
                    {block: 'a', content: [
                        {block: 'img', src: 'http://placehold.it/36x36'}
                    ]}
                ]}
            ]}
        ]}
    ]}
];
