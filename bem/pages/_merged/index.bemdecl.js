exports.blocks = [
    {
        "name": "page"
    },
    {
        "name": "page",
        "elems": [
            {
                "name": "css"
            }
        ]
    },
    {
        "name": "page",
        "elems": [
            {
                "name": "js"
            }
        ]
    },
    {
        "name": "wow"
    },
    {
        "name": "wow",
        "mods": [
            {
                "name": "type",
                "vals": [
                    {
                        "name": "longer"
                    }
                ]
            }
        ]
    },
    {
        "name": "a"
    },
    {
        "name": "img"
    },
    {
        "name": "img",
        "mods": [
            {
                "name": "responsive",
                "vals": [
                    {
                        "name": true
                    }
                ]
            }
        ]
    },
    {
        "name": "header"
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "main"
            }
        ]
    },
    {
        "name": "container"
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "inner"
            }
        ]
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "logo"
            }
        ]
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "widgets"
            }
        ]
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "user"
            }
        ]
    },
    {
        "name": "btn"
    },
    {
        "name": "btn",
        "mods": [
            {
                "name": "size",
                "vals": [
                    {
                        "name": "sm"
                    }
                ]
            }
        ]
    },
    {
        "name": "btn",
        "mods": [
            {
                "name": "color",
                "vals": [
                    {
                        "name": "link"
                    }
                ]
            }
        ]
    },
    {
        "name": "btn",
        "mods": [
            {
                "name": "underline",
                "vals": [
                    {
                        "name": "dot"
                    }
                ]
            }
        ]
    },
    {
        "name": "span"
    },
    {
        "name": "btn",
        "mods": [
            {
                "name": "color",
                "vals": [
                    {
                        "name": "primary"
                    }
                ]
            }
        ]
    },
    {
        "name": "phl"
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "nav"
            }
        ]
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "primary"
            }
        ]
    },
    {
        "name": "nav-main-menu"
    },
    {
        "name": "hidden-xs"
    },
    {
        "name": "fa"
    },
    {
        "name": "mls"
    },
    {
        "name": "nav-main-menu",
        "elems": [
            {
                "name": "dropdown"
            }
        ]
    },
    {
        "name": "header",
        "elems": [
            {
                "name": "additional"
            }
        ]
    },
    {
        "name": "nav-list-inline"
    },
    {
        "name": "nav-list-inline",
        "elems": [
            {
                "name": "dropdown"
            }
        ]
    },
    {
        "name": "main"
    },
    {
        "name": "footer"
    },
    {
        "name": "columns"
    },
    {
        "name": "columns",
        "elems": [
            {
                "name": "item"
            }
        ]
    },
    {
        "name": "h"
    },
    {
        "name": "footer",
        "elems": [
            {
                "name": "title"
            }
        ]
    },
    {
        "name": "list-unstyled"
    },
    {
        "name": "footer",
        "elems": [
            {
                "name": "list"
            }
        ]
    },
    {
        "name": "footer",
        "elems": [
            {
                "name": "dividing"
            }
        ]
    },
    {
        "name": "row"
    },
    {
        "name": "row",
        "elems": [
            {
                "name": "col"
            }
        ]
    },
    {
        "name": "row",
        "elems": [
            {
                "name": "col",
                "mods": [
                    {
                        "name": "xs",
                        "vals": [
                            {
                                "name": 12
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        "name": "row",
        "elems": [
            {
                "name": "col",
                "mods": [
                    {
                        "name": "sm",
                        "vals": [
                            {
                                "name": 6
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        "name": "social"
    },
    {
        "name": "social",
        "elems": [
            {
                "name": "title"
            }
        ]
    },
    {
        "name": "list-inline"
    },
    {
        "name": "social",
        "elems": [
            {
                "name": "list"
            }
        ]
    },
    {
        "name": "mbn"
    },
    {
        "name": "footer",
        "elems": [
            {
                "name": "bottom"
            }
        ]
    },
    {
        "name": "footer",
        "elems": [
            {
                "name": "include"
            }
        ]
    },
    {
        "name": "br"
    },
    {
        "name": "footer",
        "elems": [
            {
                "name": "include",
                "mods": [
                    {
                        "name": "type",
                        "vals": [
                            {
                                "name": "right"
                            }
                        ]
                    }
                ]
            }
        ]
    }
];
