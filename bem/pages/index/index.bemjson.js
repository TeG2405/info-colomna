module.exports = {
    block: 'page',
    title: 'Главная',
    id: 'INDEX',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [
        {elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}
    ],
    content: [
        {block: 'wow', mods: {'type': 'longer'}, content: [
            {block: 'a', content: {block: 'img', mods: {'responsive': true}, src: 'http://placehold.it/1150x180'}}
        ]},
        {block: 'header', content: [
            {elem: 'main', content: [
                {mix: {block: 'container'}, content: [
                    {elem: 'inner', content: [
                        {elem: 'logo', content: [
                            {block: 'img', mods: {'responsive': true}, src: '../../../upload/logo.png'},
                            {block: 'a'}
                        ]},
                        {elem: 'widgets', content: [
                       
                        ]},
                        {elem: 'user', content: [
                            {block: 'btn', mods: {size: 'sm', color: 'link', underline: 'dot'}, content: {block: 'span', content: 'Войти'}},
                            {block: 'btn', mods: {size: 'sm', color: 'primary'}, mix: [{block: 'phl'}], content: 'Регистрация'}
                        ]}
                    ]}
                ]},
            ]},
            {elem: 'nav', content: [
                {elem: 'primary', content: [
                    {mix: {block: 'container'}, content: [
                        {block: 'nav-main-menu', content: [
                            {block: 'a', content: 'Главная'},
                            {block: 'a', content: 'Новости'},
                            {block: 'a', content: 'Cпецпроекты'},
                            {block: 'a', content: 'Афиша'},
                            {block: 'a', content: 'Фотоотчеты'},
                            [
                                {block: 'a', mix: {block: 'hidden-xs'}, content: [
                                    'Ещё',
                                    {block: 'fa', mix: {block: 'mls'}, icon: 'angle-down'}
                                ]},
                                {elem: 'dropdown', content: [
                                    {block: 'a', content: 'Тестовый 1 Длинный пункт'},
                                    {block: 'a', content: 'Тестовый 2 Длинный пункт'},
                                    {block: 'a', content: 'Тестовый 3'}
                                ]}
                            ]
                        ]}
                    ]}
                ]},
                {elem: 'additional', content: [
                    {mix: {block: 'container'}, content: [
                        {block: 'nav-list-inline', content: [
                            {block: 'a', content: 'Недвижимость'},
                            {block: 'a', content: 'Погода'},
                            {block: 'a', content: 'Объявления'},
                            {block: 'a', content: 'Справочная'},
                            {block: 'a', content: 'Карта города'},
                            [
                                {block: 'a', mix: {block: 'hidden-xs'}, content: [
                                    'Ещё',
                                    {block: 'fa', mix: {block: 'mls'}, icon: 'angle-down'}
                                ]},
                                {elem: 'dropdown', content: [
                                    {block: 'a', content: 'Тестовый 1 Длинный пункт'},
                                    {block: 'a', content: 'Тестовый 2 Длинный пункт'},
                                    {block: 'a', content: 'Тестовый 3'}
                                ]}
                            ]
                        ]}
                    ]}
                ]}
            ]}
        ]},
        {block: 'main', content: [
            {mix: {block: 'container'}, content: [
                
            ]}
        ]},
        require('../_common/footer.bemjson.js')
    ]
};
