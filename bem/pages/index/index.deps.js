exports.deps = [
    {
        "block": "jquery"
    },
    {
        "block": "fira-sans"
    },
    {
        "block": "icomoon"
    },
    {
        "block": "pt-sans"
    },
    {
        "block": "globals"
    },
    {
        "block": "page",
        "elem": "browsehappy"
    },
    {
        "block": "conditional-comment"
    },
    {
        "block": "page",
        "elem": "console-fix"
    },
    {
        "block": "page",
        "elem": "css"
    },
    {
        "block": "page",
        "elem": "favicon"
    },
    {
        "block": "page",
        "elem": "head"
    },
    {
        "block": "page",
        "elem": "html5shiv-respond"
    },
    {
        "block": "page",
        "elem": "js"
    },
    {
        "block": "page",
        "elem": "link"
    },
    {
        "block": "page",
        "elem": "meta"
    },
    {
        "block": "page",
        "elem": "noscript"
    },
    {
        "block": "page",
        "elem": "viewport-fix"
    },
    {
        "block": "ua"
    },
    {
        "block": "html5shiv-respond"
    },
    {
        "block": "wow"
    },
    {
        "block": "wow",
        "mod": "type"
    },
    {
        "block": "wow",
        "mod": "type",
        "val": "longer"
    },
    {
        "block": "list-inline",
        "elem": "li"
    },
    {
        "block": "header",
        "elem": "main"
    },
    {
        "block": "header",
        "elem": "inner"
    },
    {
        "block": "header",
        "elem": "logo"
    },
    {
        "block": "header",
        "elem": "widgets"
    },
    {
        "block": "header",
        "elem": "user"
    },
    {
        "block": "phl"
    },
    {
        "block": "header",
        "elem": "nav"
    },
    {
        "block": "header",
        "elem": "primary"
    },
    {
        "block": "nav-main-menu"
    },
    {
        "block": "nav-main-menu",
        "elem": "li"
    },
    {
        "block": "hidden-xs"
    },
    {
        "block": "fa"
    },
    {
        "block": "mls"
    },
    {
        "block": "nav-main-menu",
        "elem": "dropdown"
    },
    {
        "block": "header",
        "elem": "additional"
    },
    {
        "block": "nav-list-inline"
    },
    {
        "block": "nav-list-inline",
        "elem": "li"
    },
    {
        "block": "nav-list-inline",
        "elem": "dropdown"
    },
    {
        "block": "main"
    },
    {
        "block": "footer"
    },
    {
        "block": "columns"
    },
    {
        "block": "columns",
        "elem": "item"
    },
    {
        "block": "footer",
        "elem": "title"
    },
    {
        "block": "list-unstyled",
        "elem": "li"
    },
    {
        "block": "footer",
        "elem": "list"
    },
    {
        "block": "footer",
        "elem": "dividing"
    },
    {
        "block": "container-fluid"
    },
    {
        "block": "row",
        "elem": "col"
    },
    {
        "block": "row",
        "elem": "col",
        "mod": "xs"
    },
    {
        "block": "row",
        "elem": "col",
        "mod": "sm"
    },
    {
        "block": "social",
        "elem": "title"
    },
    {
        "block": "social",
        "elem": "list"
    },
    {
        "block": "mbn"
    },
    {
        "block": "footer",
        "elem": "bottom"
    },
    {
        "block": "footer",
        "elem": "include"
    },
    {
        "block": "footer",
        "elem": "include",
        "mod": "type"
    },
    {
        "block": "bootstrap"
    },
    {
        "block": "typography"
    },
    {
        "block": "a"
    },
    {
        "block": "img"
    },
    {
        "block": "img",
        "mod": "responsive"
    },
    {
        "block": "img",
        "mod": "responsive",
        "val": true
    },
    {
        "block": "list-inline"
    },
    {
        "block": "container"
    },
    {
        "block": "btn"
    },
    {
        "block": "btn",
        "mod": "size"
    },
    {
        "block": "btn",
        "mod": "size",
        "val": "sm"
    },
    {
        "block": "btn",
        "mod": "color"
    },
    {
        "block": "btn",
        "mod": "color",
        "val": "link"
    },
    {
        "block": "btn",
        "mod": "underline"
    },
    {
        "block": "btn",
        "mod": "underline",
        "val": "dot"
    },
    {
        "block": "span"
    },
    {
        "block": "btn",
        "mod": "color",
        "val": "primary"
    },
    {
        "block": "h"
    },
    {
        "block": "list-unstyled"
    },
    {
        "block": "row"
    },
    {
        "block": "row",
        "elem": "col",
        "mod": "xs",
        "val": 12
    },
    {
        "block": "row",
        "elem": "col",
        "mod": "sm",
        "val": 6
    },
    {
        "block": "social"
    },
    {
        "block": "br"
    },
    {
        "block": "footer",
        "elem": "include",
        "mod": "type",
        "val": "right"
    },
    {
        "block": "space"
    },
    {
        "block": "header"
    },
    {
        "block": "page"
    }
];
